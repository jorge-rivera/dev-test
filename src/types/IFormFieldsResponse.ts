export interface IFormFieldsResponse {
  name: string;
  label: string;
  columns: number;
  required: boolean;
}
