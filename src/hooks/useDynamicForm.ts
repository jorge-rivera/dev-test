import { FormEventHandler, useEffect, useState } from "react";
import { fetchFormFields, sendForm } from "../api/form";

export const useDynamicForm = () => {
  const [formFields, setFormFields] = useState([]);
  const [valuesForm, setValuesForm] = useState({});

  // get form fields
  useEffect(() => {
    const getFormFields = async () => {
      try {
        const response = await fetchFormFields();
        setFormFields(response.data);
      } catch (e) {
        alert("something went wrong");
      }
    };

    getFormFields();
  }, []);

  // create dynamic form
  useEffect(() => {
    if (formFields) {
      const initial = {};
      formFields.forEach((field) => {
        initial[field.name] = "";
      });
      setValuesForm(initial);
    }
  }, [formFields]);

  const handleOnSubmit: FormEventHandler<HTMLFormElement> | undefined = async (
    event
  ) => {
    event.preventDefault();

    try {
      const response = await sendForm(valuesForm);
      if (response.data.success) {
        alert("Your form was submitted successfully");
      }
      if (!response.data.success) {
        alert("Sorry, form could not be processed");
      }
    } catch {
      alert("something went wrong");
    }
  };

  return { handleOnSubmit, formFields, valuesForm, setValuesForm };
};
