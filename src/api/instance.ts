import axios, { AxiosInstance } from "axios";
import { API_URL } from "../constants";

export const getInstance = async (): Promise<AxiosInstance> => {
  const baseURL = API_URL;
  const timeout = 2000;

  const request = axios.create({
    baseURL,
    timeout,
    headers: {
      "Content-Type": "application/json",
    },
  });

  return request;
};
