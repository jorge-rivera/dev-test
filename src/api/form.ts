import { AxiosError } from "axios";
import { getInstance } from "./instance";
import { FORM_ENDPOINT } from "../constants";
import { IFormValuesBody } from "../types/IFormValuesBody";

export const fetchFormFields = async () => {
  const request = await getInstance();
  const data = await request.get(FORM_ENDPOINT).catch((error: AxiosError) => {
    return {
      error,
    };
  });
  return data;
};

export const sendForm = async (formValues: IFormValuesBody | object) => {
  const request = await getInstance();
  const data = await request.post(FORM_ENDPOINT, formValues).catch((error) => {
    return {
      error,
    };
  });
  return data;
};
