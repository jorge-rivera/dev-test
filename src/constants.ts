export const API_URL = `${import.meta.env.VITE_APP_API_URL}`;
export const FORM_ENDPOINT = `${import.meta.env.VITE_APP_API_URL}/form/`;
export const INPUT_HALF = "inputHalf";
export const INPUT_FULL = "inputFull";
