import { useDynamicForm } from "../../hooks/useDynamicForm";
import Input from "../Input";
import "./style.css";

const DynamicForm = () => {
  const { handleOnSubmit, formFields, valuesForm, setValuesForm } =
    useDynamicForm();

  return (
    <div>
      <h1>Dynamic Form</h1>
      <form onSubmit={handleOnSubmit}>
        <div className='grid'>
          {formFields &&
            formFields.map((field) => (
              <Input
                field={field}
                valuesForm={valuesForm}
                setValuesForm={setValuesForm}
              />
            ))}
          <button type='submit'>Send</button>
        </div>
      </form>
    </div>
  );
};

export default DynamicForm;
