import { Dispatch, SetStateAction } from "react";
import { INPUT_FULL, INPUT_HALF } from "../../constants";
import { IFormFieldsResponse } from "../../types/IFormFieldsResponse";

const Input = ({ field, valuesForm, setValuesForm }: InputProps) => {
  const handleOnChange:
    | React.ChangeEventHandler<HTMLInputElement>
    | undefined = (event) => {
    setValuesForm((prevValuesForm) => ({
      ...prevValuesForm,
      [event.target.name]: event.target.value,
    }));
  };

  return (
    <div
      key={field.name}
      className={field.columns === 6 ? INPUT_HALF : INPUT_FULL}
    >
      <label>
        {field.label} {field.required && "(*)"}
      </label>
      <input
        name={field.name}
        required={field.required}
        value={valuesForm[field.name]}
        onChange={handleOnChange}
      />
    </div>
  );
};

type InputProps = {
  field: IFormFieldsResponse;
  valuesForm: object;
  setValuesForm: Dispatch<SetStateAction<object>>;
};

export default Input;
